<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## How to run

- Install php 7.3
- Install php7.3 mbstring
- Install php7.3 imagegick
- Install php7.3 curl
- Install php7.3 
- Install php7.3-xml


password db hosting : 127.0.0.1 / istanapompa / istanapompa / istanapompa71


php artisan migrate

php artisan config:clear
php artisan config:cache
php artisan key:generate


php artisan db:seed
php artisan make:seeder AdminUserSeeder
php artisan db:seed --class=AdminUserSeeder


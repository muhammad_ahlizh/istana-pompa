@extends('layouts.front_end')

@section('content')

<div>
    <section>
        <div class="row col-sm-12 homepage-logo-container">
            <div class="titleWrap">                
                <h1 class="subtitle">{{ isset($product) ? $product->name : '' }}</h1>
            </div>
            <div class="line"></div>
        </div>
    </section>
    <section class="about">
        <div class="container">
            <div class="row product-view">
                <div class="col-sm-12" style="text-align: right; margin-bottom: 30px;">
                    <span><a href="/">{{ $category->ParentCategory->name }}</a></span> > 
                    <span><a href="{{ '/product_category/'.$category->id }}">{{ $category->name }}</a></span> > 
                    Product 1</div>
                <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="row col-sm-12 product-detail" >
                                <div class="col-sm-4 col-xs-12" style="text-align: center;"><img src="{{ '/images/'.$product->picture_url }}" class="product-view-image"/></div>
                                <div class="col-sm-8 col-xs-12" class="product-name">
                                    <div class="col-sm-12 label-name">Name : </div>
                                    <div class="col-sm-12 label-value">{{ isset($product) ? $product->name : '' }}</div>

                                    <div class="col-sm-12 label-name">Price : </div>
                                    <div class="col-sm-12 label-value">{{ isset($product) ? 'Rp '.$product->price : '' }}</div>

                                    <div class="col-sm-12 label-name">Specification : </div>
                                    <div class="col-sm-12 label-value" style="text-align: justify;">{{ $product->specification ? $product->specification : "-" }}</div>

                                    <div class="col-sm-12 label-name">Description : </div>
                                    <div class="col-sm-12 label-value" style="text-align: justify;">{{ $product->description ? $product->description : "-" }}</div>
                                
                                    <div class="col-sm-12 label-name">Download Brochure : </div>
                                    <div class="col-sm-12 label-value">
                                        @php $no=1; @endphp
                                        @foreach($product->Brochure as $data)
                                            <a href="{{ '/images/'.$data->url }}"><span class="fa fa-file-pdf-o"> Brochure {{$no}}</span></a>
                                            <br/>
                                            @php $no++; @endphp
                                        @endforeach
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>
</div>

@stop
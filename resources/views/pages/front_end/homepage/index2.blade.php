@extends('layouts.front_end')

@section('content')

<div>
    <section id="transparentWrap"></section>
    <section id="slideContainer" style="height:500px;">
        <section id="sliderWrap">
            <div class="slideOne" style="background-color: #f2f2f2;">
                <div class="titleWrap">
                    <div class="logo"><img src="logo.png" style="max-height: auto;"/></div>
                    <h1 class="watchTitle" style="color:black; font-weight: bold;">PT ISTANA POMPA</h1>
                    <h3 class="subtitle" style="color:black;">Authorized Pump Grundfos ™ and Ebara ™ Delaer</h3>
                    <h5 class="subtitle" style="color:black;">Jl. Ngagel Madya No.71, Baratajaya, Gubeng, Kota SBY, Jawa Timur 60284</h5>
                </div>
                <div class="iphoneWrapper"></div>
            </div>
            <div class="slideTwo">
                <div class="titleWrap" style="top:6%;">
                    <div class="logo" style="width: auto;"><img src="/slider/6.jpg" style="width: 100%;"/></div>
                </div>
                <div class="watchWrap"></div>
            </div>
<!--             <div class="slideThree">
                <div class="titleWrap" style="top:5%;">
                    <div class="logo" style="width: auto;"><img src="/slider/7.jpg" style="width: 100%"/></div>
                </div>
                <div class="watchWrap"></div>
            </div> -->
            <div class="slideThree">
                <div class="titleWrap" style="top:6%;">
                    <div class="logo" style="width: auto;"><img src="/slider/7.jpg" style="width: 100%;"/></div>
                </div>
                <div class="watchWrap"></div>
            </div>
        </section>
        <div class="sliderNav">
            <a class="navOne" style="background-color: rgba(128, 128, 128, 0.8); height: 10px; width: 10px;border-radius: 50%;"></a>
            <a class="navTwo" style="background-color: rgba(128, 128, 128, 0.8); height: 10px; width: 10px;border-radius: 50%;"></a>
            <a class="navThree" style="background-color: rgba(128, 128, 128, 0.8); height: 10px; width: 10px;border-radius: 50%;"></a>
        </div>
        </div>
    </section>
    <section id="quadImageBar">
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="quadImages col-md-3 col-sm-6" id="quad1"></div>
                <div class="quadImages col-md-3 col-sm-6" id="quad2"></div>
                <div class="quadImages col-md-3 col-sm-6" id="quad3"></div>
                <div class="quadImages col-md-3 col-sm-6" id="quad4"></div> -->
            </div>
        </div>
    </section>
    <!-- <section>
        <div class="row col-sm-12 homepage-logo-container">
            <div class="titleWrap">
                <div class="logo"><img src="logo.png" style="max-height: auto;"/></div>
                <h1 class="watchTitle" style="color:black; font-weight: bold;">PT ISTANA POMPA</h1>
                <h3 class="subtitle">Authorized Pump Grundfos ™ and Ebara ™ Delaer</h3>
            </div>
            <div class="line"></div>
        </div>
    </section> -->
    <section>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="..." class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="..." class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="..." class="d-block w-100" alt="...">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    </section>
    <section>
        <div class="row grundfos-container">
            <div class="col-sm-12 title">GRUNDFOS</div>
            @if(isset($grundfos_product[0]))
                <div class="col-md-6">
                    <div>{{ $grundfos_product[0]->name }}</div>
                    <div class="learn-more">Learn more></div>
                    <img src="/images/{{ $grundfos_product[0]->picture_url }}" width="300px"/>
                </div>
            @endif
            @if(isset($grundfos_product[1]))
                <div class="col-md-6 grundfos-container">
                    <div>{{ $grundfos_product[1]->name }}</div>
                    <div class="learn-more">Learn more></div>
                    <img src="/images/{{ $grundfos_product[1]->picture_url }}" width="300px" />
                </div>
            @endif
        </div>
        <div class="line"></div>
    </section>
    <section>
        <div class="row grundfos-container">
            <div class="col-sm-12 title">EBARA</div>
            @if(isset($ebara_product[0]))
                <div class="col-md-6">
                    <div>{{ $ebara_product[0]->name }}</div>
                    <div class="learn-more">Learn more></div>
                    <img src="/images/{{ $ebara_product[0]->picture_url }}" width="300px"/>
                </div>
            @endif
            @if(isset($ebara_product[0]))
                <div class="col-md-6 grundfos-container">
                    <div>{{ $ebara_product[1]->name }}</div>
                    <div class="learn-more">Learn more></div>
                    <img src="/images/{{ $ebara_product[1]->picture_url }}" width="300px" />
                </div>
            @endif
        </div>
        <div class="line"></div>
    </section>
    <section id="whyus" style="padding: 20px 0;">
        <div class="container-fluid">
            <div class="row">
                <div style="text-align: left;" data-title-color="#443f3f" data-headings-color="#443f3f" class="panel-widget-style panel-widget-style-for-220-0-0-0">
                    <h3 class="widget-title" style="text-align: center; padding:20px;">WHY US ?</h3>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-cogs" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center;padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Berkualitas</h3>
                                <p>Kebutuhan pada pompa air adalah kebutuhan yang merupakan kelas yang banyak di butuhkan mulai dari pompa air rumah (jet pump) &amp; pompa industri untuk kebutuhan proyek besar maupun kecil maka dari itu kami adalah pilihan tepat untuk anda dalam memberikan informasi pompa yang berkualitas baik dan siap di uji.</p>
                                <p>&nbsp;</p>
                            </div><!--.info-->  
                        </div>
                    </div>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-codepen" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center; padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Strategy</h3>
                                <p>Sistem start nya pompa merupakan kemudahan anda dalam tujuan memiliki nya akan tetapi untuk mendapatkan strategy jawaban sistem yang baik pada permasalahan pompa air domestic maupun industri kami duta kreasi mulia pilihan tepat anda dalam hal tersebut.</p>
                            </div><!--.info-->  
                        </div>
                    </div>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-globe" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center;padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Jangkauan Luas</h3>
                                <p>Target kami dalam memenuhi pelanggan adalah hal yang paling kami pegang untuk tujuan memberikan kepercayaan yang baik pada anda dalam order pompa air maupun pada service pompa air maka dari itu kami duta kreasi mulia ingin memberikan hal target penjualan yang mejangkauan yang sangat luas di seluruh indonesia.</p>
                            </div><!--.info-->
                        </div>
                    </div>
                                
                </div>
            </div>
        </div>
    </section>
</div>

@stop
@extends('layouts.front_end')

@section('content')

<div>
    <section id="transparentWrap" style="height: 500px;"></section>
    <section id="slideContainer" style="height:500px;">
        <section id="sliderWrap">
            <div class="slideOne" style="background-color: #f2f2f2;">
                <div class="titleWrap">
                    <div class="logo"><img src="logo.png" style="max-height: auto;"/></div>
                    <h1 class="watchTitle" style="color:black; font-weight: bold;">PT ISTANA POMPA</h1>
                    <h3 class="subtitle" style="color:black;">Authorized Pump Grundfos ™ and Ebara ™ Delaer</h3>
                    <h5 class="subtitle" style="color:black;">Jl. Ngagel Madya No.71, Baratajaya, Gubeng, Kota SBY, Jawa Timur 60284</h5>
                </div>
                <div class="iphoneWrapper"></div>
            </div>
            <div class="slideTwo">
                <div class="titleWrap" style="top:6%;">
                    <div class="logo" style="width: auto;"><img src="/slider/6.jpg" style="width: 100%;"/></div>
                </div>
                <div class="watchWrap"></div>
            </div>
<!--             <div class="slideThree">
                <div class="titleWrap" style="top:5%;">
                    <div class="logo" style="width: auto;"><img src="/slider/7.jpg" style="width: 100%"/></div>
                </div>
                <div class="watchWrap"></div>
            </div> -->
            <div class="slideThree">
                <div class="titleWrap" style="top:6%;">
                    <div class="logo" style="width: auto;"><img src="/slider/7.jpg" style="width: 100%;"/></div>
                </div>
                <div class="watchWrap"></div>
            </div>
        </section>
        <div class="sliderNav">
            <a class="navOne" style="background-color: rgba(128, 128, 128, 0.8); height: 10px; width: 10px;border-radius: 50%;"></a>
            <a class="navTwo" style="background-color: rgba(128, 128, 128, 0.8); height: 10px; width: 10px;border-radius: 50%;"></a>
            <a class="navThree" style="background-color: rgba(128, 128, 128, 0.8); height: 10px; width: 10px;border-radius: 50%;"></a>
        </div>
        </div>
    </section>
    <div class="row col-sm-12" style="background-color: #333; height: 10px;"></div>
    @foreach($grundfos_product as $data)
        <section>        
            <div class="row col-sm-12" style="text-align: center; color: white; background-color: #000; padding: 20px 0;">
                <h1 style="text-transform: uppercase; margin: auto; font-size: 32px; line-height: 1.125; font-weight: 600; letter-spacing: .004em; margin-bottom: .2em; font-family: "SF Pro Display","SF Pro Icons","Helvetica Neue","Helvetica","Arial",sans-serif;">{{ $data->name }}</h1>
                <h3 style="text-transform: capitalize;">{{ $data->description }}</h3>
                <a style="cursor:pointer;color: #6bf;" data-toggle="modal" data-target="#grundfos{{ $data->id }}"><h4>Learn More > </h4></a>
                @if (count($data->brochure) > 0)
                    <img data-toggle="modal" data-target="#grundfos{{ $data->id }}" src="{{ url('/images/products/' . $data->brochure[0]->url) }}" style="height: 250px;" />
                @endif
            </div>        

            <div class="container">
                <!-- Modal -->
                <div id="grundfos{{ $data->id }}" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="text-transform: uppercase;">{{ $data->name }}</h4>
                        </div>
                        <div class="modal-body">

                            <embed src="{{ url('/images/' .$data->picture_url) }}"
                                   frameborder="0" width="100%" height="400px">

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <div class="row col-sm-12" style="background-color: #333; height: 10px;"></div>
    @endforeach
   <!--  <section>
        <div class="row col-sm-12" style="text-align: center; color: #000; background-color: white; padding: 20px 0;">
            <h1 style="margin: auto; font-size: 32px; line-height: 1.125; font-weight: 600; letter-spacing: .004em; margin-bottom: .2em; font-family: "SF Pro Display","SF Pro Icons","Helvetica Neue","Helvetica","Arial",sans-serif;">EBARA </h1>
            <h3>For Engineers Who Like To Move The Limits</h3>
            <a href="#" style="color: #0070c9; font-size: 21px; line-height: 1.38105; font-weight: 400; letter-spacing: .011em; font-family: "SF Pro Display","SF Pro Icons","Helvetica Neue","Helvetica","Arial",sans-serif;"><h4>Learn More > </h4></a>
            <img src="/products/1.png" style="height: 250px;" />
        </div>
    </section> -->
     <section>
        <div class="row grundfos-container">
            <div class="col-sm-12 title"></div>
                @foreach($ebara_product as $data)
                    <div class="col-md-6">
                        <div><h1 style="text-transform: uppercase; margin: auto; font-size: 32px; line-height: 1.125; font-weight: 600; letter-spacing: .004em; margin-bottom: .2em; font-family: "SF Pro Display","SF Pro Icons","Helvetica Neue","Helvetica","Arial",sans-serif;">{{ $data->name }} </h1></div>
                        <div class="learn-more"><a href="#" style="color: #0070c9; font-size: 21px; line-height: 1.38105; font-weight: 400; letter-spacing: .011em; font-family: "SF Pro Display","SF Pro Icons","Helvetica Neue","Helvetica","Arial",sans-serif;cursor:pointer;" data-toggle="modal" data-target="#ebara{{$data->id}}"><h4>Learn More > </h4></a></div>
                        @if(count($data->brochure) > 0)
                            <img src="{{ url('/images/products/' .$data->brochure[0]->url )}}" width="300px" data-toggle="modal" data-target="#ebara{{$data->id}}"/>
                        @endif
                    </div>
                @endforeach
                

                @foreach($ebara_product as $data)
                    <div class="container">
                        <!-- Modal -->
                        <div id="ebara{{$data->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title" style="text-transform: uppercase;">{{$data->name}}</h4>
                                </div>
                                <div class="modal-body">

                                    <embed src="{{ url('/images/' . $data->picture_url) }}"
                                           frameborder="0" width="100%" height="400px">

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
        </div>
        <div class="line"></div>
    </section>
  
    <section id="whyus" style="padding: 20px 0;">
        <div class="container-fluid">
            <div class="row">
                <div style="text-align: left;" data-title-color="#443f3f" data-headings-color="#443f3f" class="panel-widget-style panel-widget-style-for-220-0-0-0">
                    <h3 class="widget-title" style="text-align: center; padding:20px;">WHY US ?</h3>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-cogs" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center;padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Berkualitas</h3>
                                <p>Kebutuhan pada pompa air adalah kebutuhan yang merupakan kelas yang banyak di butuhkan mulai dari pompa air rumah (jet pump) &amp; pompa industri untuk kebutuhan proyek besar maupun kecil maka dari itu kami adalah pilihan tepat untuk anda dalam memberikan informasi pompa yang berkualitas baik dan siap di uji.</p>
                                <p>&nbsp;</p>
                            </div><!--.info-->
                        </div>
                    </div>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-codepen" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center; padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Strategy</h3>
                                <p>Sistem start nya pompa merupakan kemudahan anda dalam tujuan memiliki nya akan tetapi untuk mendapatkan strategy jawaban sistem yang baik pada permasalahan pompa air domestic maupun industri kami duta kreasi mulia pilihan tepat anda dalam hal tersebut.</p>
                            </div><!--.info-->
                        </div>
                    </div>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-globe" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center;padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Jangkauan Luas</h3>
                                <p>Target kami dalam memenuhi pelanggan adalah hal yang paling kami pegang untuk tujuan memberikan kepercayaan yang baik pada anda dalam order pompa air maupun pada service pompa air maka dari itu kami duta kreasi mulia ingin memberikan hal target penjualan yang mejangkauan yang sangat luas di seluruh indonesia.</p>
                            </div><!--.info-->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <div id="dialog" style="display: none">
</div>

@stop
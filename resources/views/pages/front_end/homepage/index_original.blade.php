@extends('layouts.front_end')

@section('content')

<div>
    <section id="transparentWrap"></section>
    <section id="slideContainer">
        <section id="sliderWrap">
            <div class="slideOne">
                <div class="titleWrap">
                    <div class="logo"><img src="logo.png" style="max-height: auto;filter: brightness(0) invert(1);"/></div>
                    <h1 class="watchTitle" style="color:#6bf">PT ISTANA POMPA</h1>
                    <h3 class="subtitle">Authorized Pump Grundfos ™ and Ebara ™ Delaer</h3>
                </div>
                <div class="iphoneWrapper"></div>
            </div>
            <div class="slideTwo">
                <div class="titleWrap">
                    <span class="editionButton" style="border: 1px solid black;border-radius: 5px;padding: 5px;margin-bottom: 10px;">Special Edition</span>
                    <div class="logo" style="margin-top:20px"><img src="images/ebara.jpg" style="max-height: 250px"/></div>
                    <h1 class="watchTitle" style="padding-top:20%">Pompa Ebara CDX</h1>
                    <a>See Detail ></a>
                </div>
                <div class="watchWrap"></div>
            </div>
            <div class="slideThree">
                <div class="titleWrap">
                    <h1>iPad</h1>
                    <h2>Flat-out fun.</h2>
                </div>
                <div class="ipadWrap"></div>
            </div>
        </section>
        <div class="sliderNav">
            <a class="navOne">One</a>
            <a class="navTwo">Two</a>
            <a class="navThree">Three</a>
        </div>
    </section>
    <section id="quadImageBar">
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="quadImages col-md-3 col-sm-6" id="quad1"></div>
                <div class="quadImages col-md-3 col-sm-6" id="quad2"></div>
                <div class="quadImages col-md-3 col-sm-6" id="quad3"></div>
                <div class="quadImages col-md-3 col-sm-6" id="quad4"></div> -->
            </div>
        </div>
    </section>
    <section id="whyus" style="padding: 20px 0;">
        <div class="container-fluid">
            <div class="row">
                <div style="text-align: left;" data-title-color="#443f3f" data-headings-color="#443f3f" class="panel-widget-style panel-widget-style-for-220-0-0-0">
                    <h3 class="widget-title" style="text-align: center; padding:20px;">Why Us ?</h3>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-cogs" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center;padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Berkualitas</h3>
                                <p>kebutuhan pada pompa air adalah kebutuhan yang merupakan kelas yang banyak di butuhkan mulai dari pompa air rumah (jet pump) &amp; pompa industri untuk kebutuhan proyek besar maupun kecil maka dari itu kami adalah pilihan tepat untuk anda dalam memberikan informasi pompa yang berkualitas baik dan siap di uji.</p>
                                <p>&nbsp;</p>
                            </div><!--.info-->  
                        </div>
                    </div>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-codepen" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center; padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Strategy</h3>
                                <p>sistem start nya pompa merupakan kemudahan anda dalam tujuan memiliki nya akan tetapi untuk mendapatkan strategy jawaban sistem yang baik pada permasalahan pompa air domestic maupun industri kami duta kreasi mulia pilihan tepat anda dalam hal tersebut.</p>
                            </div><!--.info-->  
                        </div>
                    </div>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-globe" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center;padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Jangkauan Luas</h3>
                                <p>target kami dalam memenuhi pelanggan adalah hal yang paling kami pegang untuk tujuan memberikan kepercayaan yang baik pada anda dalam order pompa air maupun pada service pompa air maka dari itu kami duta kreasi mulia ingin memberikan hal target penjualan yang mejangkauan yang sangat luas di seluruh indonesia.</p>
                            </div><!--.info-->
                        </div>
                    </div>
                                
                </div>
            </div>
        </div>
    </section>
</div>

@stop
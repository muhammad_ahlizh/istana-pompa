@extends('layouts.front_end')

@section('content')

<div>
    <section>
        <div class="row col-sm-12 homepage-logo-container">
            <div class="titleWrap">                
                <h1 class="subtitle">{{ isset($category) ? $category->name : ''}}</h1>
            </div>
            <div class="line"></div>
        </div>
    </section>
    <section class="about">
        <div class="container">
            <div class="row product-list">
                <div class="col-sm-2">
                    <div class="item-list-header">{{ isset($category) ? $category->ParentCategory->name : ''}}</div>
                    @foreach($category_list as $data)
                        <div class="item-list">
                            <a href="{{ '/product_category/'.$data->id }}" style="text-decoration: none; color:black;">{{ $data->name }}</a>
                        </div>
                    @endforeach
                </div>
                <div class="col-sm-10">
                @if(count($product) > 0)
                        @foreach($product as $data)
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="row col-sm-12 product-detail" >
                                    <a href="{{ '/product/'.$data->id }}" style="color:black; text-decoration: none;">
                                        <div><img src="{{ '/images/'.$data->picture_url }}" width="150px"/></div>
                                        <div class="product-name">{{ $data->name }}</div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @else 
                       <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="row col-sm-12 noproduct" >
                                No products found
                            </div>
                        </div>
                    @endif 
                </div>

                <div class="row col-sm-12 col-xs-12 product-pagination">
                    {{ $product->links() }}
                </div>
            </div>
        </div>
    </section>
</div>

@stop
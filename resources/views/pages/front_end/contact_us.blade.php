@extends('layouts.front_end')

@section('content')

<div>
    <section>
        <div class="row col-sm-12 homepage-logo-container">
            <div class="titleWrap">                
                <h1 class="subtitle">Contact Our Company</h1>
            </div>
            <div class="line"></div>
        </div>
    </section>
    <section class="about">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 description"> {!! $contact_us->description !!} </div>
            </div>
        </div>
    </section>
</div>

@stop
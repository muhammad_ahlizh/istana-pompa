@extends('layouts.admin')

@section('content')

<div>
  <div class="">
    <div class="page-title">
      <div class="title_left">
        @if (isset($master_category))
          <h3>Edit Product</h3>
        @else
          <h3>Add New Product</h3>
        @endif        
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Product Form</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          
          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          <div class="x_content">
            <br />
            @if (isset($master_category))
              {{ Form::model($master_product, array('route' => array('master_product.update', $master_product->id), 'method' => 'PUT', 'class'=>'form-horizontal form-label-left')) }}
              <input type="hidden" id="id" name="id"  class="form-control col-md-7 col-xs-12" placeholder="e.g Hand Tool" value="{{ isset($master_category) ? $master_category->id : null }}">
            @else
              <form enctype="multipart/form-data" method="post" action="/master_product/save" class="form-horizontal form-label-left">
            @endif
              {{ csrf_field() }}
              <div class="form-group" style="display: none;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Unique ID <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="unique_id" name="unique_id" required="required" class="form-control col-md-7 col-xs-12" value="{{ isset($master_product) ? $master_product->unique_id : $unique_id }}" disabled>
                  <input type="hidden" id="unique_id" name="unique_id" required="required" class="form-control col-md-7 col-xs-12" value="{{ isset($master_product) ? $master_product->unique_id : $unique_id }}">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Parent Category <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control col-md-7 col-xs-12 dynamic" name="parent_category_id" id="parent_category_id" data-dependent="category" required="required">
                    <option>Choose Parent Category</option>
                    @foreach($parent_category as $data)
                      <option value="{{ $data->id }}"
                          {{ isset($master_category) ? ( ($master_category->parent_category_id == $data->id) ? 'selected' : null) : null }}>{{ $data->name }}</option>                      
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control col-md-7 col-xs-12" name="category" id="category" required="required">                      
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12" placeholder="e.g Grundfos GX 95" value="{{ isset($master_product) ? $master_product->name : null }}" required>
                </div>
              </div>

              <!-- <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Price <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">              
                  <input type="number" id="price" name="price" required="required" class="form-control col-md-7 col-xs-12" placeholder="e.g 100000" value="{{ isset($master_product) ? $master_product->price : null }}" required>
                </div>
              </div> -->

               <!-- <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Specification <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea class="form-control" rows="5" name="specification"></textarea>
                </div>
              </div> -->

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Description</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea class="form-control" rows="5" name="description"></textarea>
                </div>
              </div>

              <!-- <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Image <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="file" name="product_image" />
                </div>
              </div> -->

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Brochure <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="file" name="product_brochure" id="product_brochure" required="required" />
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button type="submit" class="btn btn-success">Submit</button>
                  <button class="btn btn-primary" type="reset"><a href="/master_product" style="text-decoration: none; color: white;">Back</a></button>
                </div>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>

  </div>


</div>

<script>
$(document).ready(function(){

 $('.dynamic').change(function(){
  if($(this).val() != '')
  {
   var select = $(this).attr("id");
   var value = $(this).val();
   var dependent = $(this).data('dependent');
   var _token = $('input[name="_token"]').val();
   $.ajax({
    url:"{{ route('dynamic_category.fetch') }}",
    method:"POST",
    data:{select:select, value:value, _token:_token, dependent:dependent},
    success:function(result)
    {
     $('#'+dependent).html(result);
    }

   })
  }
 });

 $('#parent_category_id').change(function(){
  $('#category').val('');
 });
 

});
</script>
@stop
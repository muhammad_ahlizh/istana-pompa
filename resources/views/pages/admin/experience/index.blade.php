@extends('layouts.admin')

@section('content')
<div>

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Our Experience Page </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="row">
          <div class="col-sm-10">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <span>{{ session('status') }}</span>
                </div>
              
            @endif  

            @if (session('warning'))
              
                <div class="alert alert-warning alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <span>{{ session('warning') }}</span>
                </div>
              
            @endif
          </div>
          {{ Form::open(['url' => 'master_experience','method' => 'post','class'=>'form-horizontal form-label-left']) }}
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <textarea id="description" name="description" placeholder="Type here" autofocus>{{ $experience->description }}</textarea>
              </div>
            </div>
            <br/>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          {{ Form::close() }}
          <br/>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
$('#description').trumbowyg({
    btns: [['viewHTML'],
        ['undo', 'redo'], // Only supported in Blink browsers
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        ['insertImage'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']],

    autogrow: true
});
</script>

@stop
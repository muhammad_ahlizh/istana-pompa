@extends('layouts.admin')

@section('content')

<div>

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Category of Product</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="row">
          <div class="col-sm-10">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <span>{{ session('status') }}</span>
                </div>
              
            @endif  

            @if (session('warning'))
              
                <div class="alert alert-warning alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <span>{{ session('warning') }}</span>
                </div>
              
            @endif
          </div>
          <div class="col-sm-2" style="text-align: right;">
            <button class="btn btn-primary btn-sm" type="button"><a href="/master_category/create" style="text-decoration: none; color: white;">New Category of Product</a></button>
          </div>

        </div>
        <br/>
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>Category Name</th>
              <th>Category Group</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @php $no=1 @endphp

            @foreach($master_category as $data)
              <tr>
                <td>{{ $no }}</td>
                <td>{{ $data->name }}</td>
                <td>{{ $data->ParentCategory->name }}</td>
                <td>
                  <a class="btn btn-warning btn-xs" href="{{ '/master_category/'.$data->id }}"> 
                    <span class="fa fa-desktop"></span> Detail
                  </a>
                  <a class="btn btn-info btn-xs" href="{{ '/master_category/'.$data->id.'/edit' }}"> 
                    <span class="fa fa-pencil"></span> Edit
                  </a>
                  {{ Form::open(array('url' => 'master_category/' . $data->id, 'style' => 'display:inline')) }}
                      {{ Form::hidden('_method', 'DELETE') }}
                      <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash"></i> Delete</button>
                  {{ Form::close() }}
                </td>
              </tr>
              @php $no++ @endphp
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
@stop
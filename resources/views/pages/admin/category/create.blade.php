@extends('layouts.admin')

@section('content')

<div>
  <div class="">
    <div class="page-title">
      <div class="title_left">
        @if (isset($master_category))
          <h3>Edit Parent Category</h3>
        @else
          <h3>Add New Category</h3>
        @endif        
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Category Form</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          
          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          <div class="x_content">
            <br />
            @if (isset($master_category))
              {{ Form::model($master_category, array('route' => array('master_category.update', $master_category->id), 'method' => 'PUT', 'class'=>'form-horizontal form-label-left')) }}
              <input type="hidden" id="id" name="id"  class="form-control col-md-7 col-xs-12" placeholder="e.g Hand Tool" value="{{ isset($master_category) ? $master_category->id : null }}">
            @else
              {{ Form::open(['url' => 'master_category','method' => 'post','class'=>'form-horizontal form-label-left']) }}
            @endif
              {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="name" name="name"  class="form-control col-md-7 col-xs-12" placeholder="e.g Hand Tool" value="{{ isset($master_category) ? $master_category->name : null }}" required autofocus>
                  @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Group <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control col-md-7 col-xs-12" name="parent_category_id">
                    <option>Choose Category Group</option>
                    @foreach($parent_category as $data)
                      <option value="{{ $data->id }}"
                          {{ isset($master_category) ? ( ($master_category->parent_category_id == $data->id) ? 'selected' : null) : null }}>{{ $data->name }}</option>                      
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button type="submit" class="btn btn-success">Submit</button>
                  <button class="btn btn-primary" type="reset"><a href="/master_category" style="text-decoration: none; color: white;">Back</a></button>
                </div>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>

  </div>


</div>
@stop
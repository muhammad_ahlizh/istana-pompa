@extends('layouts.admin')

@section('content')

<div>
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Category Detail</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Category Information</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Name</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <label class="control-label" for="first-name">: {{ $master_category ? $master_category->name : null }}</label>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Group</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <label class="control-label" for="first-name">: {{ $master_category ? $master_category->ParentCategory->name : null }}</label>
                </div>
              </div>


            </form>
          </div>
        </div>

        <div class="x_panel">
          <div class="x_title">
            <h2>Sub Category </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="col-sm-12" style="text-align: right;">
            <button class="btn btn-primary btn-sm" type="button"><a href="{{ '/master_category/create_sub_category/'.$master_category->id }}" style="text-decoration: none; color: white;">New Sub Category</a></button>
          </div>
          <div class="x_content">
            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Sub Category</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @php $no=1 @endphp

                @foreach($sub_category as $data)
                  <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $data->name }}</td>
                    <td>
                      <a class="btn btn-info btn-xs" href="{{ '/master_sub_category/'.$data->id.'/edit' }}"> 
                        <span class="fa fa-pencil"></span> Edit
                      </a>
                      <a class="btn btn-danger btn-xs" href="{{ '/master_sub_category/'.$data->id.'/delete' }}"> 
                        <span class="fa fa-trash"></span> Delete
                      </a>
                    </td>
                  </tr>
                  @php $no++ @endphp
                @endforeach
              </tbody>
          </table>
            
          </div>
        </div>
      </div>

    </div>

  </div>


</div>
@stop
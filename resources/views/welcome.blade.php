<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Istana Pompa</title>
    <meta name="description" content="Apple website clone">
    <meta name="author" content="Zach Mitchell">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/css/styles.css">
</head>
<body>
    <header>
        <nav>
            <ul class="mobileNav">
                <li><a class="fa fa-bars"></a></li>
                <li><a class="fa fa-apple"></a></li>
                <li><a id="shoppingBag"></a></li>
            </ul>
            <ul>
                <li class="fa fa-home"></li>
                <li>Products</li>
                <li>Find Us</li>
                <li>About Me</li>
                <li>Advertisement</li>
                <li><a id="searchIcon"></a></li>
                <li><a id="shoppingBag"></a></li>
            </ul>
        </nav>
    </header>
    <section id="transparentWrap"></section>
    <section id="slideContainer">
        <section id="sliderWrap">
            <div class="slideOne">
                <div class="titleWrap">
                    <div class="logo"><img src="logo.png" style="max-height: auto;filter: brightness(0) invert(1);"/></div>
                    <h1 class="watchTitle" style="color:#6bf">PT ISTANA POMPA</h1>
                    <h3 class="subtitle">Authorized Pump Grundfos ™ and Ebara ™ Delaer</h3>
                </div>
                <div class="iphoneWrapper"></div>
            </div>
            <div class="slideTwo">
                <div class="titleWrap">
                    <span class="editionButton" style="border: 1px solid black;border-radius: 5px;padding: 5px;margin-bottom: 10px;">Special Edition</span>
                    <div class="logo" style="margin-top:20px"><img src="images/ebara.jpg" style="max-height: 250px"/></div>
                    <h1 class="watchTitle" style="padding-top:20%">Pompa Ebara CDX</h1>
                    <a>See Detail ></a>
                </div>
                <div class="watchWrap"></div>
            </div>
            <div class="slideThree">
                <div class="titleWrap">
                    <h1>iPad</h1>
                    <h2>Flat-out fun.</h2>
                </div>
                <div class="ipadWrap"></div>
            </div>
        </section>
        <div class="sliderNav">
            <a class="navOne">One</a>
            <a class="navTwo">Two</a>
            <a class="navThree">Three</a>
        </div>
    </section>
    <section id="quadImageBar">
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="quadImages col-md-3 col-sm-6" id="quad1"></div>
                <div class="quadImages col-md-3 col-sm-6" id="quad2"></div>
                <div class="quadImages col-md-3 col-sm-6" id="quad3"></div>
                <div class="quadImages col-md-3 col-sm-6" id="quad4"></div> -->
            </div>
        </div>
    </section>
    <section id="whyus" style="padding: 20px 0;">
        <div class="container-fluid">
            <div class="row">
                <div style="text-align: left;" data-title-color="#443f3f" data-headings-color="#443f3f" class="panel-widget-style panel-widget-style-for-220-0-0-0">
                    <h3 class="widget-title" style="text-align: center; padding:20px;">Why Us ?</h3>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-cogs" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center;padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Berkualitas</h3>
                                <p>kebutuhan pada pompa air adalah kebutuhan yang merupakan kelas yang banyak di butuhkan mulai dari pompa air rumah (jet pump) &amp; pompa industri untuk kebutuhan proyek besar maupun kecil maka dari itu kami adalah pilihan tepat untuk anda dalam memberikan informasi pompa yang berkualitas baik dan siap di uji.</p>
                                <p>&nbsp;</p>
                            </div><!--.info-->  
                        </div>
                    </div>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-codepen" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center; padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Strategy</h3>
                                <p>sistem start nya pompa merupakan kemudahan anda dalam tujuan memiliki nya akan tetapi untuk mendapatkan strategy jawaban sistem yang baik pada permasalahan pompa air domestic maupun industri kami duta kreasi mulia pilihan tepat anda dalam hal tersebut.</p>
                            </div><!--.info-->  
                        </div>
                    </div>
                    <div class="service col-md-4">
                        <div class="roll-icon-box">
                            <div class="icon" style="text-align: center;"><i class="fa fa-globe" style="color: #6bf;font-size: 26px;border: 1px solid #6bf;border radius: ;border-radius: 50%;padding: 20px;"></i></div>
                            <div class="content" style="text-align: center;padding:20px;">
                                <h3 style="color: rgb(68, 63, 63);">Jangkauan Luas</h3>
                                <p>target kami dalam memenuhi pelanggan adalah hal yang paling kami pegang untuk tujuan memberikan kepercayaan yang baik pada anda dalam order pompa air maupun pada service pompa air maka dari itu kami duta kreasi mulia ingin memberikan hal target penjualan yang mejangkauan yang sangat luas di seluruh indonesia.</p>
                            </div><!--.info-->
                        </div>
                    </div>
                                
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">Shop and Learn</div>
                <div class="col-md-2 col-md-offset-1">
                    <ul>
                        <li>Shop and Learn</li>
                        <li>Mac</li>
                        <li>iPad</li>
                        <li>iPhone</li>
                        <li>Watch</li>
                        <li>TV</li>
                        <li>Music</li>
                        <li>iTunes</li>
                        <li>iPod</li>
                        <li>Accessories</li>
                        <li>Gift Cards</li>
                    </ul>
                </div>
                <div class="col-sm-12">Apple Store</div>
                <div class="col-md-2">
                    <ul>
                        <li>Apple Store</li>
                        <li>Find a Store</li>
                        <li>Genius Bar</li>
                        <li>Workshops and Learning</li>
                        <li>Youth Programs</li>
                        <li>Apple Store App</li>
                        <li>Refurbished and Clearance</li>
                        <li>Financing</li>
                        <li>Reuse and Recycling</li>
                        <li>Order Status</li>
                        <li>Shopping Help</li>
                    </ul>
                </div>
                <div class="col-sm-12">For Education</div>
                <div class="col-sm-12">For Business</div>
                <div class="col-md-2">
                    <ul>
                        <li>For Education</li>
                        <li>Apple and Education</li>
                        <li>Shop for College</li>
                    </ul>
                    <ul>
                        <li>For Business</li>
                        <li>Apple and Business</li>
                        <li>Shop for Business</li>
                    </ul>
                </div>
                <div class="col-sm-12">Account</div>
                <div class="col-sm-12">Apple Values</div>
                <div class="col-md-2">
                    <ul>
                        <li>Account</li>
                        <li>Manage Your Apple ID</li>
                        <li>Apple Store Account</li>
                        <li>iCloud.com</li>
                    </ul>
                    <ul>
                        <li>Apple Values</li>
                        <li>Accessibility</li>
                        <li>Education</li>
                        <li>Environment</li>
                        <li>Inclusion and Diversity</li>
                        <li>Privacy</li>
                        <li>Supplier Responsibility</li>
                    </ul>
                </div>
                <div class="col-sm-12">About Apple</div>
                <div class="col-md-2">
                    <ul>
                        <li>About Apple</li>
                        <li>Apple Info</li>
                        <li>Newsroom</li>
                        <li>Job Opportunities</li>
                        <li>Press Info</li>
                        <li>Investors</li>
                        <li>Events</li>
                        <li>Contact Apple</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="footerWrap col-md-10 col-md-offset-1">
                    <p>More ways to shop: Visit an <a>Apple Store</a>, call 1-800-MY-APPLE, or <a>find a reseller</a>.</p>
                    <div id="footerLegal">
                        <div class="copyright col-md-4">
                            <p>Copyright © 2017 Apple Inc. All rights reserved.</p>
                        </div>
                        <div class="legalLinks col-md-6">
                            <a href="#">Privacy Policy</a>
                            <a href="#">Terms of Use</a>
                            <a href="#">Sales and Refunds</a>
                            <a href="#">legal</a>
                            <a href="#">Site Map</a>
                        </div>
                        <div class="footerLocal col-md-2">
                            <span id="flagPNG"></span>
                            <a>United States</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script></body>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>

    @include('includes.admin.head')

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          
          @include('includes.admin.left_menu')

          @include('includes.admin.top_navigation')

          <!-- page content -->
          <div class="right_col" role="main">
            @yield('content')
          </div>
          <!-- /page content -->

          @include('includes.admin.footer')          
      </div>
    </div>

    @include('includes.admin.footer_js')

  </body>
</html>

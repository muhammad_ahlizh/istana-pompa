<!DOCTYPE html>
<html>
<head>
    @include('includes.front_end.head')
</head>
<body>
    <header>
        @include('includes.front_end.top_menu')
    </header>
    <div>
      @yield('content')
    </div>
    
    @include('includes.front_end.footer')

    @include('includes.front_end.footer_js')
</body>
</html>
<div class="left_col scroll-view">
  <div class="navbar nav_title" style="border: 0;">
    <p class="site_title"><span style="font-size: 17px;">Istana Pompa</span></p>
  </div>

  <div class="clearfix"></div>
    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="/images/img_male.png" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2>{{ Auth::user()->name }}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Main Menu</h3>
        <ul class="nav side-menu">
          <li class="{{isset($route) ? (($route == 'master_category') ? 'current-page' : '') : ''}}"><a href="/master_category"><i class="fa fa-sitemap"></i> Master Product Category</a></li>
          <li class="{{isset($route) ? (($route == 'master_product') ? 'current-page' : '') : ''}}"><a href="/master_product"><i class="fa fa-cube"></i> Master Products</a></li>
          <li class="{{isset($route) ? (($route == 'master_aboutus') ? 'current-page' : '') : ''}}"><a href="/master_aboutus"><i class="fa fa-bank"></i>About Us Page</a></li>
          <li class="{{isset($route) ? (($route == 'master_contactus') ? 'current-page' : '') : ''}}"><a href="/master_contactus"><i class="fa fa-phone"></i>Contact Us Page</a></li>
          <li class="{{isset($route) ? (($route == 'master_experience') ? 'current-page' : '') : ''}}"><a href="/master_experience"><i class="fa fa-history"></i>Our Experience Page</a></li>
        </ul>
      </div>
    </div>
    <!-- /sidebar menu -->  
  </div>
</div>
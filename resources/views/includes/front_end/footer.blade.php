<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">Ebara</div>
            <div class="col-md-2 col-md-offset-1">
                <ul>
                    <li>Ebara</li>
                    @foreach($ebara_category as $data)
                        <li>{{ $data->name }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-12">Grundfos</div>
            <div class="col-md-2">
                <ul>
                    <li>Grundfos</li>
                    @foreach($grundfos_category as $data)
                        <li>{{ $data->name }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-12">About Us</div>
            <div class="col-md-2">
                <ul>
                    <li>About Us</li>
                </ul>
            </div>
            <div class="col-sm-12">Contact Us</div>
            <div class="col-md-2">
                <ul>
                    <li>Contact Us</li>
                </ul>
            </div>
            <div class="col-sm-12">Our Experince</div>
            <div class="col-md-2">
                <ul>
                    <li>Our Experience</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="footerWrap col-md-10 col-md-offset-1">
                <p>More ways to shop: <br/>
                    Visit an PT Istana Pompa Store Jl. Ngagel Madya No.71, Baratajaya, Gubeng, Kota SBY, Jawa Timur 60284<br/>
                    Call  (031) 502 6677 - 9, Email : istanapompa@gmail.com</p>
                <div id="footerLegal">
                    <div class="copyright col-md-4">
                        <p>Copyright © {{ date('Y') }} PT Istana Pompa. All rights reserved.</p>
                    </div>
                    <div class="legalLinks col-md-6">
                        <a href="#">Privacy Policy</a>
                        <a href="#">Terms of Use</a>
                        <a href="#">Legal</a>
                        <a href="#">Site Map</a>
                    </div>
                    <div class="footerLocal col-md-2">
                        <img src="/images/flag.png"/ width="20px">
                        <a>Indonesia</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
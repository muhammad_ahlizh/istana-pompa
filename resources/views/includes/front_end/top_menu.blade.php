<nav style="color: rgba(45,45,45,0.98);">
    <ul class="mobileNav">
        <li><a class="fa fa-bars" onclick="mobileMenu()"></a></li>
        <li style="margin: 0px auto;"><img src="/logo.png" width="25px" height="30px" style="filter:brightness(0) invert(1);" /></li>
    </ul>
    
    <ul>
        <li class="menu"><a class="{{ ($route == 'home') ? 'active' : 'notactive' }}" href="/"><img src="/logo.png" width="25px" height="27px" style="filter:brightness(0) invert(1);" /></a></li>
        <li onClick="showHideEbara()" style="color:white;">Ebara</li>
        <li onClick="showHideGrundfos()" style="color:white;">Grundfos</li>
        <li class="menu"><a class="{{ ($route == 'about_us') ? 'active' : 'notactive' }}" href="/about_us">About Us</a></li>
        <li class="menu"><a class="{{ ($route == 'contact_us') ? 'active' : 'notactive' }}" href="/contact_us">Contact Us</a></li>
        <li class="menu"><a class="{{ ($route == 'our_experience') ? 'active' : 'notactive' }}" href="/our_experience">Our Experience</a></li>
        <!-- <li><a id="searchIcon"></a></li> -->
        <!-- <li><a id="shoppingBag"></a></li> -->
    </ul>
</nav>
<nav id="mobileMenu" style="display: none; margin-top:48px; height: 270px">
    <div> 
        <div class="row col-sm-12 menu" style="margin:0px 15px; color: white;"><a class="{{ ($route == 'home') ? 'active' : 'notactive' }}" href="/">Home</a></div>
        <div class="row col-sm-12 menu" style="margin:0px 15px;color: white;">
            <a onclick="mobileSubMenuEbara({{ count($ebara_category)}})" class="notactive">Ebara</a>
        </div>
        <div class="row sub-menu" style="display: none;" id="sub-menu-ebara">
            @foreach($ebara_category as $data)
                <div class="row" style="margin-left:40px;"><a href="{{ '/product_category/'.$data->id }}" 
                    style="color:black; text-decoration: none; color: white;"> {{ $data->name }} </a></div>
            @endforeach
        </div>
        <div class="row col-sm-12 menu" style="margin:0px 15px; color: white;">
            <a onclick="mobileSubMenuGrundfos({{count($grundfos_category)}})" class="notactive">Grundfos</a>
        </div>
        <div class="row sub-menu" style="display: none;" id="sub-menu-grundfos">
            <!-- @foreach($grundfos_category as $data)
                <div class="row" style="margin-left:40px;">{{ $data->name }}</div>    
            @endforeach() -->
             @foreach($grundfos_category as $data)
                <div class="row" style="margin-left:40px;"><a href="{{ '/product_category/'.$data->id }}" style="color:black; text-decoration: none; color: white;"> {{ $data->name }} </a></div>
            @endforeach
        </div>
        
        <div class="row col-sm-12 menu" style="margin:0px 15px"><a class="{{ ($route == 'about_us') ? 'active' : 'notactive' }}" href="/about_us">About Us</a></div>
        <div class="row col-sm-12 menu" style="margin:0px 15px"><a class="{{ ($route == 'contact_us') ? 'active' : 'notactive' }}" href="/contact_us">Contact Us</a></div>
        <div class="row col-sm-12 menu" style="margin:0px 15px"><a class="{{ ($route == 'our_experience') ? 'active' : 'notactive' }}" href="/our_experience">Our Experience</a></div>
    </div>
</nav>
<nav id="ebaraMenu" style="margin-top: 44px; display: none; height: 45px; background-color: #ffffffde; color: black;">
    <div class="container" style="padding: 0 100px;">
        <div class="row" style="border-bottom: 0.5px solid #d6d6d6;">
            <div class="col-sm-4">Ebara</div>
            <div class="col-sm-8 container">
                <div class="row justify-content-end">
                    @foreach($ebara_category as $data)
                        <div class="col-sm-2"><a href="{{ '/product_category/'.$data->id }}" style="color:black; text-decoration: none;"> {{ $data->name }} </a></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</nav>
<nav id="grundfosMenu" style="margin-top: 44px; display: none; height: 45px; background-color: #ffffffde; color: black;">
    <div class="container" style="padding: 0 100px;">
        <div class="row" style="border-bottom: 0.5px solid #d6d6d6;">
            <div class="col-sm-4">Grundfos</div>
            <div class="col-sm-8 container">
                <div class="row justify-content-end">
                    @foreach($grundfos_category as $data)
                        <div class="col-sm-2"><a href="{{ '/product_category/'.$data->id }}" style="color:black; text-decoration: none;"> {{ $data->name }} </a></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</nav>
<script type="text/javascript">

    function mobileMenu() {
        var x = document.getElementById("mobileMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    function mobileSubMenuEbara(count) {
        // alert(count);
        var x = document.getElementById("sub-menu-ebara");
        var container = document.getElementById('mobileMenu');
        var height = container.clientHeight;
        if (x.style.display === "none") {            
            var new_height = height + (count * 40);
            container.style.height = new_height + "px";
            x.style.display = "block";
        } else {
            var new_height = height - (count * 40);
            container.style.height = new_height + "px";
            x.style.display = "none";
        }
    }

    function mobileSubMenuGrundfos(count) {
        var x = document.getElementById("sub-menu-grundfos");
        var container = document.getElementById('mobileMenu');
        var height = container.clientHeight;
        if (x.style.display === "none") {            
            var new_height = height + (count * 40);
            container.style.height = new_height + "px";
            x.style.display = "block";
        } else {
            var new_height = height - (count * 40);
            container.style.height = new_height + "px";
            x.style.display = "none";
        }
    }

    function showHideEbara() {
        // $(document).ready(function(){
        //     $("#ebaraMenu").toggle(1000);
        // });
        document.getElementById("grundfosMenu").style.display = "none";
        var x = document.getElementById("ebaraMenu");
        if (x.style.display === "none") {
            x.style.display = "";
        } else {
            x.style.display = "none";
        }
    }

    function showHideGrundfos() {
        // $(document).ready(function(){
        //     $("#ebaraMenu").toggle(1000);
        // });
        document.getElementById("ebaraMenu").style.display = "none";
        var x = document.getElementById("grundfosMenu");
        if (x.style.display === "none") {
            x.style.display = "";
        } else {
            x.style.display = "none";
        }
    }

    function showMobile() {
        $(document).ready(function(){
            $("#mobile").toggle(1000);
        });
    }    
</script>
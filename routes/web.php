<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEndController@index')->name('home');
Route::get('/about_us', 'FrontEndController@AboutUs')->name('about_us');
Route::get('/contact_us', 'FrontEndController@contactUs')->name('contact_us');
Route::get('/our_experience', 'FrontEndController@OurExperience')->name('our_experience');
Route::get('/product_category/{category_id}', 'FrontEndController@ListProduct')->name('list_product');
Route::get('/product/{id}', 'FrontEndController@DetailProduct')->name('detail_product');

Auth::routes();
Route::get('/admin', 'HomeController@index')->name('master_category');

// MASTER CATEGORY
Route::resource('master_category', 'CategoryController', ['names' => ['index' => 'master_category', 'create' => 'master_category', 'show' => 'master_category', 'edit' => 'master_category']]);

Route::get('master_category/create_sub_category/{category_id}', 'CategoryController@CreateSubCategory')->name('sub_category');
Route::post('master_sub_category/store_sub_category', 'CategoryController@StoreSubCategory')->name('sub_category');
Route::get('master_sub_category/{sub_category_id}/edit', 'CategoryController@EditSubCategory')->name('sub_category');
Route::post('master_sub_category/update_sub_category', 'CategoryController@UpdateSubCategory')->name('sub_category');
Route::get('master_sub_category/{sub_category_id}/delete', 'CategoryController@DeleteSubCategory')->name('sub_category');

// MASTER PRODUCT
Route::resource('master_product', 'ProductController', ['names' => ['index' => 'master_product', 'create' => 'master_product', 'show' => 'master_product', 'edit' => 'master_product']]);
Route::post('category_data/fetch', 'ProductController@GetCategoryAjax')->name('dynamic_category.fetch');

Route::post('master_product/save', 'ProductController@store')->name('master_product');

Route::resource('master_aboutus', 'AboutUsController', ['names' => ['index' => 'master_aboutus', 'create' => 'master_aboutus', 'show' => 'master_aboutus', 'edit' => 'master_aboutus']]);

Route::resource('master_contactus', 'ContactUsController', ['names' => ['index' => 'master_contactus', 'create' => 'master_contactus', 'show' => 'master_contactus', 'edit' => 'master_contactus']]);

Route::resource('master_experience', 'ExperienceController', ['names' => ['index' => 'master_experience', 'create' => 'master_experience', 'show' => 'master_experience', 'edit' => 'master_experience']]);
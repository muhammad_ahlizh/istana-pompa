<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->string('name');
            // $table->unsignedInteger('sub_category_id');
            $table->unsignedInteger('category_id');
            $table->decimal('price',12,2)->default(0);
            $table->decimal('discount',12,2)->default(0);
            $table->string('picture_url')->nullable();
            $table->timestamps();
            $table->softdeletes();

            // $table->foreign('sub_category_id')->references('id')->on('sub_category')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}

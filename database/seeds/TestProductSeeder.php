<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class TestProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 4; $i < 25; $i++) {
            $product = Product::create([
                'name'      => 'Test Product '.$i,
                'unique_id' => 'A'.$i,
                'category_id' => 1,
                'price' => 1500000,
                'discount' => 0,
                'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
            ]);            
        }

       //  $product = Product::create([
       //      'name' 		=> 'Test Product',
       //      'unique_id' => 'A1',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       //  $product = Product::create([
       //      'name' 		=> 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       //  $product = Product::create([
       //      'name' 		=> 'Test Product 3',
       //      'unique_id' => 'A3',
       //      'category_id' => 2,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       //  $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A4',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       //  $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       //  $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       //  $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       // $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       // $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       // $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       // $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

       // $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);
       // $product = Product::create([
       //      'name'      => 'Test Product 2',
       //      'unique_id' => 'A2',
       //      'category_id' => 1,
       //      'price' => 1500000,
       //      'discount' => 0,
       //      'picture_url' => 'https://www.pumpsukltd.com/media/catalog/product/e/b/ebara_dwo_open_impeller_pumps_1_1_1.jpg'
       //  ]);

    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\ParentCategory;

class ParentCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parent_category = ParentCategory::create([
            'name' => 'Ebara'
        ]);

        $parent_category = ParentCategory::create([
            'name' => 'Grundfos'
        ]);
    }
}

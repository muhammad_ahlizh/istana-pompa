<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Route;

// Models
use App\Models\ParentCategory;
use App\Models\AboutUs;
use App\Models\ContactUs;
use App\Models\Experience;
use App\Models\Category;
use App\Models\Product;

class FrontEndController extends Controller
{
    public function index()
    {
    	$parent_category = ParentCategory::get();
    	$route = Route::currentRouteName();

    	$ebara_category = Category::GetEbaraCategory();
    	$grundfos_category = Category::GetGrundfosCategory();

		$ebara_product = Product::join('category', 'category.id', 'product.category_id')
								->where('category.parent_category_id', '1')
								->select('product.*')
								->with('Brochure')
								->inRandomOrder()
								->take(2)
								->get();

		$grundfos_product = Product::join('category', 'category.id', 'product.category_id')
								->where('category.parent_category_id', '2')
								->select('product.*')
								->with('Brochure')
								->inRandomOrder()
								->take(2)
								->get();
		
        return view('pages.front_end.homepage.index')
        		->with('parent_category', $parent_category)
        		->with('route', $route)
        		->with('ebara_category', $ebara_category)
        		->with('grundfos_category', $grundfos_category)
        		->with('ebara_product', $ebara_product)
        		->with('grundfos_product', $grundfos_product);
    }

	public function AboutUs()
	{
		$about_us = AboutUs::first();
		$parent_category = ParentCategory::get();
		$route = Route::currentRouteName();
		$ebara_category = Category::GetEbaraCategory();
    	$grundfos_category = Category::GetGrundfosCategory();

		return view('pages.front_end.about_us')
				->with('about_us', $about_us)
				->with('parent_category', $parent_category)
				->with('route', $route)
				->with('ebara_category', $ebara_category)
        		->with('grundfos_category', $grundfos_category);
	}

	public function ContactUs()
	{
		$contact_us = ContactUs::first();
		$parent_category = ParentCategory::get();
		$route = Route::currentRouteName();
		$ebara_category = Category::GetEbaraCategory();
    	$grundfos_category = Category::GetGrundfosCategory();

		return view('pages.front_end.contact_us')
				->with('contact_us', $contact_us)
				->with('parent_category', $parent_category)
				->with('route', $route)
				->with('ebara_category', $ebara_category)
        		->with('grundfos_category', $grundfos_category);
	}

	public function OurExperience()
	{
		$our_experience = Experience::first();
		$parent_category = ParentCategory::get();
		$route = Route::currentRouteName();
		$ebara_category = Category::GetEbaraCategory();
    	$grundfos_category = Category::GetGrundfosCategory();

		return view('pages.front_end.our_experience')
				->with('our_experience', $our_experience)
				->with('parent_category', $parent_category)
				->with('route', $route)
				->with('ebara_category', $ebara_category)
        		->with('grundfos_category', $grundfos_category);
	}

	public function ListProduct($category_id)
	{
		$our_experience = Experience::first();
		$parent_category = ParentCategory::get();
		$route = Route::currentRouteName();

		$ebara_category = Category::GetEbaraCategory();
    	$grundfos_category = Category::GetGrundfosCategory();

		$category = Category::with('ParentCategory')
							->where('id', $category_id)
							->first();

		$category_list = [];
		if ($category) {
			if ($category->ParentCategory->name == 'Ebara') {
				$category_list = $ebara_category;
			} else if ($category->ParentCategory->name == 'Grundfos') {
				$category_list = $grundfos_category;
			}	
		}

		$product = Product::where('category_id', $category_id)->paginate(9);
		

		return view('pages.front_end.list_product')
				->with('our_experience', $our_experience)
				->with('parent_category', $parent_category)
				->with('route', $route)
				->with('ebara_category', $ebara_category)
        		->with('grundfos_category', $grundfos_category)
        		->with('category_list', $category_list)
        		->with('category', $category)
        		->with('product', $product);
	}

	public function DetailProduct($id)
	{
		$our_experience = Experience::first();
		$parent_category = ParentCategory::get();
		$route = Route::currentRouteName();

		$ebara_category = Category::GetEbaraCategory();
    	$grundfos_category = Category::GetGrundfosCategory();
			
    	$product = Product::with('Brochure')->find($id);

    	$category_data = Category::find($product->category_id);
		return view('pages.front_end.product_detail')
				->with('our_experience', $our_experience)
				->with('parent_category', $parent_category)
				->with('route', $route)
				->with('ebara_category', $ebara_category)
        		->with('grundfos_category', $grundfos_category)
        		->with('product', $product)
        		->with('category', $category_data);
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use Redirect;
use Route;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

// MODELS
use App\Models\Product;
use App\Models\ParentCategory;
use App\Models\Category;
use App\Models\Brochure;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $master_product = Product::join('category', 'product.category_id', 'category.id')
                                ->join('parent_category', 'category.parent_category_id', 'parent_category.id')
                                ->select('product.*', 'parent_category.name as parent_category')
                                ->orderBy('updated_at', 'DESC')
                                ->with('Category')
                                ->get();

        $route = Route::currentRouteName();

        return view('pages.admin.product.index')
                    ->with('master_product', $master_product)
                    ->with('route', $route);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $route = Route::currentRouteName();

        $parent_category = ParentCategory::get();

        $unique_id = Product::CreateUniqueId();

        return view('pages.admin.product.create')
                    ->with('route', $route)
                    ->with('parent_category', $parent_category)
                    ->with('unique_id', $unique_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name'      => 'required',
            'category'    => 'required',
            'unique_id' => 'required'
        ]);

        // $file_brochure = $request->file("product_brochure"); //file src

        // return gettype($file_brochure);

        DB::beginTransaction();
        try {

            $product = new Product;
            $product->name = $request->name;
            $product->unique_id = $request->unique_id;
            $product->category_id = $request->category;
            $product->price = $request->price ? $request->price : 0 ;
            $product->specification = $request->specification ? $request->specification : " " ;
            $product->description = $request->description;
            $product->save();

            // $file_src=$request->file("product_image"); //file src
            // $is_file_uploaded = Storage::disk('public_uploads')->put('products', $file_src);

            // if ($is_file_uploaded) {
            //     $product->picture_url = $is_file_uploaded;
            //     $product->save();
            // }

            // if ($is_file_uploaded) {
            //     $product->picture_url = $is_file_uploaded;
            //     $product->save();
            // }

            // upload brochure
            $file_brochure = $request->file("product_brochure"); //file src
            $is_file_uploaded = Storage::disk('public_uploads')->put('brochure', $file_brochure);
            if ($is_file_uploaded) {
                $product->picture_url = $is_file_uploaded;
                $product->save();
            }

            // $pathToWhereImageShouldBeStored = "convert";
            if ($is_file_uploaded) {
                $pdf = new \Spatie\PdfToImage\Pdf('images/'.$is_file_uploaded);
                $page = $pdf->getNumberOfPages();

                for ($i=0; $i < $page; $i++) {
                    // 
                    $pathToWhereImageShouldBeStored = "images/products/".$product->unique_id."-".$i.".jpg";
                    $pdf->setPage($i+1)
                        ->saveImage($pathToWhereImageShouldBeStored);

                    // if ($is_file_uploaded) {
                        $brochure = new Brochure;
                        $brochure->url = $product->unique_id."-".$i.".jpg";
                        $brochure->product_id = $product->id;
                        $brochure->save();
                    // }
                }
            }

            DB::commit();

            Session::flash('status', 'Successfully added new data');
        } catch (\Exception $e) {

            Session::flash('warning', 'Error '.$e->getMessage());
            // something went wrong
        }

        return Redirect::to('master_product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::with('Brochure')->find($id);

        $route = Route::currentRouteName();

        $parent_category = ParentCategory::get();

        return view('pages.admin.product.edit')
                    ->with('route', $route)
                    ->with('parent_category', $parent_category)
                    ->with('master_product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'      => 'required',
            'category'    => 'required',
            'unique_id' => 'required'
        ]);

        // $file_brochure = $request->file("product_brochure"); //file src

        // return gettype($file_brochure);

        DB::beginTransaction();
        try {

            $product = Product::find($request->id);
            $product->name = $request->name;
            $product->unique_id = $request->unique_id;
            $product->category_id = $request->category;
            $product->price = $request->price;
            $product->specification = $request->specification;
            $product->description = $request->description;
            $product->save();

            // $file_src=$request->file("product_image"); //file src
            // if ($file_src) {
            //     $is_file_uploaded = Storage::disk('public_uploads')->put('products', $file_src);

            //     if ($is_file_uploaded) {
            //         $product->picture_url = $is_file_uploaded;
            //         $product->save();
            //     }

            //     if ($is_file_uploaded) {
            //         $product->picture_url = $is_file_uploaded;
            //         $product->save();
            //     }
            // }

            // upload brochure
            $file_brochure = $request->file("product_brochure"); //file src

            for ($i=0; $i < count($file_brochure); $i++) {
                $is_file_uploaded = Storage::disk('public_uploads')->put('brochure', $file_brochure[$i]);

                if ($is_file_uploaded) {
                    $brochure = new Brochure;
                    $brochure->url = $is_file_uploaded;
                    $brochure->product_id = $product->id;
                    $brochure->save();
                }
            }

            DB::commit();

            Session::flash('status', 'Successfully added new data');
        } catch (\Exception $e) {

            Session::flash('warning', 'Error '.$e->getMessage());
            // something went wrong
        }

        return Redirect::to('master_product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        Session::flash('status', 'Successfully delete data');
        return Redirect::to('master_product');
    }

    // AJAX DYNAMIC CATEGORY
    function GetCategoryAjax(Request $request)
    {
     $select = $request->get('select');
     $value = $request->get('value');
     $dependent = $request->get('dependent');

     // return $select."-".$value."-".$dependent;
     $data = Category::where("parent_category_id", $value)->get();

     $output = '<option value="">Select '.ucfirst($dependent).'</option>';
     foreach($data as $row)
     {
      $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
     }
     echo $output;
    }
}

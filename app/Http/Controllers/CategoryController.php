<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use Redirect;
use Route;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Models\Roles;
use App\Models\ParentCategory;
use App\Models\Category;
use App\Models\SubCategory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $master_category = Category::with('ParentCategory')
                                ->orderBy('updated_at', 'DESC')
                                ->get();

        $route = Route::currentRouteName();

        return view('pages.admin.category.index')
                    ->with('master_category', $master_category)
                    ->with('route', $route);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $route = Route::currentRouteName();

        $parent_category = ParentCategory::get();

        return view('pages.admin.category.create')
                    ->with('route', $route)
                    ->with('parent_category', $parent_category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name'      => 'required',
            'parent_category_id' => 'required'
        ]);

        DB::beginTransaction();
        try {

            $category = new Category;
            $category->name = $request->name;
            $category->parent_category_id = $request->parent_category_id;
            $category->save();
            DB::commit();

            Session::flash('status', 'Successfully added new data');
        } catch (\Exception $e) {

            Session::flash('warning', 'Error '.$e->getMessage());
            // something went wrong
        }

        return Redirect::to('master_category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $route = Route::currentRouteName();
        $master_category = Category::with('ParentCategory')
                                ->find($id);

        $sub_category = SubCategory::where('category_id', $master_category->id)->get();

        return view('pages.admin.category.show')
                ->with('route', $route)
                ->with('master_category', $master_category)
                ->with('sub_category', $sub_category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $route = Route::currentRouteName();
        $category = Category::find($id);

        $parent_category = ParentCategory::get();

        return view('pages.admin.category.create')
                    ->with('route', $route)
                    ->with('parent_category', $parent_category)
                    ->with('master_category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'id'    => 'required',
            'name'  => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('master_category')
                ->withErrors($validator);
        } else {

            DB::beginTransaction();
            try {
                // CREATE USER FOR SALES
                $category = Category::find($id);
                $category->name = $request->name;
                $category->parent_category_id = $request->parent_category_id;
                $category->save();

                DB::commit();

                Session::flash('status', 'Successfully updated data');
            } catch (\Exception $e) {

                Session::flash('warning', 'Error '.$e->getMessage());
                // something went wrong
            }

            return Redirect::to('master_category');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // CHECK DATA
        $category = Category::find($id);
        $category->delete();

        Session::flash('status', 'Successfully delete data');
        return Redirect::to('master_category');
    }

    public function CreateSubCategory($category_id)
    {
        $route = Route::currentRouteName();

        $category = Category::with('ParentCategory')
                            ->where('id', $category_id)
                            ->first();

        return view('pages.admin.category.create_sub_category')
                    ->with('route', $route)
                    ->with('master_category', $category);
    }

    public function StoreSubCategory(Request $request)
    {
        $validatedData = $request->validate([
            'name'      => 'required',
            'parent_category_id' => 'required',
            'category_id' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $sub_category = new SubCategory;
            $sub_category->name = $request->name;
            $sub_category->category_id = $request->category_id;
            $sub_category->save();
            DB::commit();

            Session::flash('status', 'Successfully added new data');
        } catch (\Exception $e) {

            Session::flash('warning', 'Error '.$e->getMessage());
            // something went wrong
        }

        return Redirect::to('master_category/'.$request->category_id);
    }

    public function EditSubCategory($sub_category_id)
    {
        $route = Route::currentRouteName();
        $sub_category = SubCategory::find($sub_category_id);

        $category = Category::with('ParentCategory')
                            ->where('id', $sub_category->category_id)
                            ->first();

        return view('pages.admin.category.create_sub_category')
                    ->with('route', $route)
                    ->with('master_category', $category)
                    ->with('master_sub_category', $sub_category);
    }

    public function UpdateSubCategory(Request $request)
    {
        $rules = array(
            'sub_category_id'    => 'required',
            'name'               => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('master_sub_category/' . $request->sub_category_id . '/edit')
                ->withErrors($validator);
        } else {

            DB::beginTransaction();
            try {
                // CREATE USER FOR SALES
                $sub_category = SubCategory::find($request->sub_category_id);
                $sub_category->name = $request->name;
                $sub_category->save();

                DB::commit();

                Session::flash('status', 'Successfully updated data');
            } catch (\Exception $e) {

                Session::flash('warning', 'Error '.$e->getMessage());
                // something went wrong
            }

            return Redirect::to('master_category/'.$request->category_id);
        }
    }

    public function DeleteSubCategory($sub_category_id)
    {
        $route = Route::currentRouteName();
        $sub_category = SubCategory::find($sub_category_id);

        $category_id = $sub_category->category_id;

        if ($sub_category) {
            DB::beginTransaction();
            try {
                // CREATE USER FOR SALES
                $sub_category->delete();

                DB::commit();

                Session::flash('status', 'Successfully delete data');
            } catch (\Exception $e) {

                Session::flash('warning', 'Error '.$e->getMessage());
                // something went wrong
            }
        }

        return Redirect::to('master_category/' . $category_id);
    }
}

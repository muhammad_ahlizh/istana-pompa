<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// HELPERS
use App\Helpers\StringHelper;

class Product extends Model
{
    protected $table = 'product';

    public function Category()
	{
		return $this->belongsTo('App\Models\Category', 'category_id', 'id');
	}

    public static function CreateUniqueId()
	{
		$unique_id = StringHelper::guid('PRO', 8, 1);;
        $check_unique_id = Product::CheckUniqueId($unique_id);
        while ($check_unique_id) {
            $uniqe_id = StringHelper::guid('PRO', 8, 1);;
            $check_unique_id = Product::CheckUniqueId($unique_id);
        }

        return $unique_id;
	}

	public static function CheckUniqueId($code)
    {
        $data = Product::where('unique_id', $code)->first();

        return $data;
    }

    public function Brochure()
    {
        return $this->hasMany('App\Models\Brochure');
    }
}

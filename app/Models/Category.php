<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

	public function ParentCategory()
	{
		return $this->belongsTo('App\Models\ParentCategory', 'parent_category_id', 'id');
	}

	public static function GetEbaraCategory() 
	{
		$ebara = Category::where('parent_category_id', 1)
						->with('ParentCategory')
						->limit(5)
						->orderBy('id', 'ASC')
						->get();
		return $ebara;
	}

	public static function GetGrundfosCategory()
	{
		$grundfos = Category::where('parent_category_id', 2)
						->with('ParentCategory')
						->limit(5)
						->orderBy('id', 'ASC')
						->get();
		return $grundfos;
	}

}